import React from 'react';
import './styles/main.scss';
import Wheel from './components/wheel';
import Arrow from './components/arrow';
import Button from './components/button';
import Winner from './components/winner';

export default class WheelFortuneApp extends React.Component {

  listPrize = [
    'Ice bal',
    'Fire bal',
    'Human brain',
    'Human heart',
    'Dance party',
    'Russian vodka',
    'Beer',
    'Milk',
    'Potatoes',
    'Rainbow bird',
    'Clear water',
    'Blood women',
  ];

  angle = 0;
  container = React.createRef();
  winContainer = React.createRef();

  maxValue = 25;
  dataSet = [];
  sectorsElemProp = [];
  
  state = {
    winner: false,
    winnerNumber: 0,
    spinned: false,
    tokens: 3,
    sectors: 4,
    sectorsElem: []
  };

  randNum = (min, max, check) => {
    if(!check)
      return Math.round(Math.random() * (max - min) + min);
    else
      return Math.random() * (max - min) + min;
  };
  
	randColor = () => {
    var r = this.randNum(0,255);
    var g = this.randNum(0,255);
    var b = this.randNum(0,255);
    return "rgb("+r+","+g+","+b+")";
  };

  calcHorde = (e) => {
    const value = Number(e.target.value);
    this.setState({
      sectors: value
    }, () => {
      this.generateWheel();
    });
  };

  move = () => {
    const containerWheel = this.container.current;
    const winContainer = this.winContainer.current;
    const { sectors } = this.state;

    this.angle += this.randNum(365, 2920);
    const winSectorNum = (sectors - Math.ceil((this.angle % 360) / (360 / sectors))) + 1;

    containerWheel.style.transform = `rotateZ(${this.angle}deg)`;
    winContainer.innerHTML = winSectorNum;

    setTimeout(() => {
      if (winSectorNum > 9) {
        winContainer.setAttribute('x', '36%');
        winContainer.setAttribute('y', '60%');
      } else {
        winContainer.setAttribute('x', '43%');
        winContainer.setAttribute('y', '60%');
      }
      this.setState({
        spinned: false,
        winner: true,
        winnerNumber: winSectorNum
      });
    }, 5000);

  };

  spin = () => {
    const { tokens, spinned } = this.state;
    if (!spinned && tokens > 0) {
      this.setState({
        tokens: tokens - 1,
        spinned: true,
        winner: false
      }, () => {
        this.move();
      });
    }
  };

  generateWheel = () => {
    const { sectors } = this.state;

    if (!sectors) {
      return;
    }

    this.sectorsElemProp = [];
    this.dataSet = [];

    const value = 100/sectors;

    for (let i = 1; i <= sectors; i++) {
      const color = this.randColor();
      this.dataSet.push({value, color, text: i});
    }

    this.dataSet.reduce((prev, curr) => {
      return this.addPart(curr, prev);
    }, 0);

    this.setState({
      sectorsElem: this.sectorsElemProp
    })
  };

  addPart = (data, angle) => {
    if (data.value <= this.maxValue) {
      return this.addSector(data, angle, false);
    }

    return this.addPart({
      value: data.value - this.maxValue,
      color: data.color,
      text: data.text
    }, this.addSector({
      value: this.maxValue,
      color: data.color,
      text: data.text,
    }, angle, true));
  };

  addSector = (data, startAngle, collapse) => {
    let sectorDeg = 3.6 * data.value;
    let skewDeg = 90 + sectorDeg;
    let rotateDeg = startAngle;
    if (collapse) {
      skewDeg++;
    }

    this.sectorsElemProp.push({
      classElem: `sector sector${data.text}`,
      backgroundColor: data.color,
      transform: `rotate(${rotateDeg}deg) skewY(${skewDeg}deg)`
    });
    return startAngle + sectorDeg;
  };

  render(){
    const { winner, tokens, spinned, winnerNumber, sectors, sectorsElem } = this.state;
    const prize = this.listPrize[winnerNumber%this.listPrize.length];
    return(
      <>
        <div className="wheel-fortune">
          <div className="wheel-fortune-inner">
            <Arrow />
            <Wheel sectorsElem={sectorsElem} generateWheel={this.generateWheel} ref={this.container} />
            {(!spinned && !winner) && (
              <>
                <div className="row row__input">
                  <input type="number" value={sectors} min={4} onChange={this.calcHorde} />
                </div>
                <div className="row row__button">
                  <Button clickHandler={this.spin} title="Spin" disabled={sectors < 2} />
                </div>
              </>
            )}
          </div>
        </div>
        <Winner
          clickHandler={this.spin}
          tokens={tokens}
          winner={winner}
          winnerNumber={winnerNumber}
          prize={prize}
          ref={this.winContainer}
        />
      </>
    )
  };
}
