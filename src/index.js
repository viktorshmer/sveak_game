import React from 'react';
import ReactDOM from 'react-dom';
import WheelFortuneApp from './WheelFortuneApp';

ReactDOM.render(<WheelFortuneApp />, document.getElementById('root'));