import React from 'react';

function Sector({classElem, backgroundColor, transform}) {
  return (
    <div className={classElem} style={{backgroundColor: backgroundColor, transform: transform}} />
  );
}
export default Sector;
