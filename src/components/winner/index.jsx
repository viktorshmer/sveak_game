import React from 'react';
import './index.scss';
import WinSvg from '../win_icon_svg';

import Button from '../button';

const Winner = React.forwardRef(({clickHandler, tokens, winner, winnerNumber, prize}, ref) => {
  const titleBtn = `Spin as well! (${tokens} tokens)`;
  const winClass = `winner ${winner ? 'show' : ''}`;
  const animClass = winner ? 'animation-buble' : '';
  return(
    <div className={winClass}>
      <div className="left-col">
        <div className="wheel-min">
          <WinSvg animClass={animClass} ref={ref}/>
        </div>
      </div>
      <div className="right-col">
        <div className="right-col__title">Wheel of Fortune: hugoboss spinned {winnerNumber}!</div>
        <div className="right-col__prize">Prize: {prize}</div>
        {(tokens > 0) && 
          <div className="right-col__button">
            <Button clickHandler={clickHandler} title={titleBtn} />
          </div>
        }
      </div>
    </div>
  );
});

export default Winner;