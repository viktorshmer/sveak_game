import React from 'react';

const WinSvg = React.forwardRef(({animClass}, ref) => (
  <svg className={animClass} width="81" height="80" viewBox="0 0 81 80" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M73.5 40C73.5 58.5015 58.5015 73.5 40 73.5C21.4985 73.5 6.5 58.5015 6.5 40C6.5 21.4985 21.4985 6.5 40 6.5C58.5015 6.5 73.5 21.4985 73.5 40Z" fill="#FFF3D4" stroke="#FEE39F"/>
    <g filter="url(#filter0_d)">
    <ellipse cx="40" cy="40.0002" rx="31.5122" ry="31.5122" fill="white"/>
    </g>
    <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="9" y="9" width="62" height="62">
    <circle cx="40" cy="40" r="31" fill="#97C21E"/>
    </mask>
    <g mask="url(#mask0)">
    <path d="M69.9436 48.0232C68.5171 53.3472 65.6968 58.1946 61.7735 62.0659L39.9999 39.9998L69.9436 48.0232Z" fill="#42C521"/>
    <path d="M61.9202 61.9201C58.0228 65.8176 53.1567 68.6054 47.8233 69.9964L39.9999 39.9998L61.9202 61.9201Z" fill="#31D6D6"/>
    <path d="M48.0233 69.9435C42.6993 71.3701 37.0912 71.3514 31.7769 69.8893L39.9999 39.9998L48.0233 69.9435Z" fill="#51ABEC"/>
    <path d="M31.9765 69.9435C26.6525 68.517 21.8051 65.6967 17.9338 61.7734L39.9999 39.9998L31.9765 69.9435Z" fill="#5157EC"/>
    <path d="M18.0796 61.9201C14.1822 58.0227 11.3943 53.1566 10.0033 47.8232L39.9999 39.9998L18.0796 61.9201Z" fill="#7951EC"/>
    <path d="M10.0562 48.0232C8.62963 42.6992 8.64835 37.0911 10.1104 31.7768L39.9999 39.9998L10.0562 48.0232Z" fill="#B151EC"/>
    <path d="M10.0562 31.9764C11.4828 26.6524 14.303 21.805 18.2264 17.9337L39.9999 39.9998L10.0562 31.9764Z" fill="#EC51CA"/>
    <path d="M18.0796 18.0795C21.977 14.1821 26.8431 11.3942 32.1765 10.0032L39.9999 39.9998L18.0796 18.0795Z" fill="#EC5151"/>
    <path d="M31.9765 10.0561C37.3005 8.62955 42.9086 8.64826 48.2229 10.1103L39.9999 39.9998L31.9765 10.0561Z" fill="#D02929"/>
    <path d="M48.0233 10.0561C53.3473 11.4827 58.1947 14.3029 62.066 18.2263L39.9999 39.9998L48.0233 10.0561Z" fill="#D98C19"/>
    <path d="M61.9202 18.0795C65.8177 21.9769 68.6055 26.843 69.9965 32.1764L39.9999 39.9998L61.9202 18.0795Z" fill="#D6B81B"/>
    <path d="M69.9436 31.9764C71.3702 37.3004 71.3515 42.9085 69.8894 48.2228L39.9999 39.9998L69.9436 31.9764Z" fill="#97C21E"/>
    </g>
    <g opacity="0.4" filter="url(#filter1_f)">
    <ellipse cx="40.4111" cy="40.0191" rx="9.73387" ry="9.55769" fill="#28313E"/>
    </g>
    <circle cx="40" cy="40" r="15" fill="black" />
    <text id="win-text-svg" x="41%" y="62%" fill="white" fontSize="17pt" ref={ref}></text>
    <ellipse cx="40.1253" cy="7.41579" rx="0.415793" ry="0.415795" fill="url(#paint0_linear)"/>
    <ellipse cx="23.9225" cy="11.7259" rx="0.415795" ry="0.415794" transform="rotate(-30 23.9225 11.7259)" fill="url(#paint1_linear)"/>
    <ellipse rx="0.415794" ry="0.415794" transform="matrix(0.499999 -0.866026 0.866025 0.500001 11.7257 23.6454)" fill="url(#paint2_linear)"/>
    <ellipse cx="7.41579" cy="39.8479" rx="0.415796" ry="0.415794" transform="rotate(-90 7.41579 39.8479)" fill="url(#paint3_linear)"/>
    <ellipse rx="0.415797" ry="0.415795" transform="matrix(-0.499996 -0.866028 0.866023 -0.500004 12.0033 56.0775)" fill="url(#paint4_linear)"/>
    <ellipse rx="0.415793" ry="0.415795" transform="matrix(-0.866024 -0.500002 0.499998 -0.866027 23.9225 68.2743)" fill="url(#paint5_linear)"/>
    <ellipse cx="40.1242" cy="72.5842" rx="0.415795" ry="0.415796" transform="rotate(-180 40.1242 72.5842)" fill="url(#paint6_linear)"/>
    <ellipse rx="0.415792" ry="0.415793" transform="matrix(-0.866023 -0.500004 -0.499996 0.866027 56.3544 11.7259)" fill="url(#paint7_linear)"/>
    <ellipse rx="0.415794" ry="0.415793" transform="matrix(-0.499998 -0.866027 -0.866024 0.500002 68.2738 23.6454)" fill="url(#paint8_linear)"/>
    <ellipse rx="0.415796" ry="0.415793" transform="matrix(0 -1 -1 0 72.5842 39.8479)" fill="url(#paint9_linear)"/>
    <ellipse rx="0.415796" ry="0.415792" transform="matrix(0.499993 -0.866029 -0.866021 -0.500007 68.2736 56.0775)" fill="url(#paint10_linear)"/>
    <ellipse rx="0.41579" ry="0.415793" transform="matrix(0.866021 -0.500007 -0.499993 -0.866029 56.3543 68.2743)" fill="url(#paint11_linear)"/>
    <g filter="url(#filter2_d)">
    <path fillRule="evenodd" clipRule="evenodd" d="M42.8284 4.20324C43.5785 4.97366 43.9999 6.01857 43.9999 7.10811L44 7.10811L40 11L36.0001 7.10811L36 7.10811C36 6.01857 36.4214 4.97366 37.1716 4.20324C37.9217 3.43282 38.9391 3 40 3C41.0608 3 42.0782 3.43282 42.8284 4.20324Z" fill="#FEC22D"/>
    </g>
    <defs>
    <filter id="filter0_d" x="8.48779" y="8.48804" width="63.0243" height="64.0244" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
    <feOffset dy="1"/>
    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.3 0"/>
    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
    </filter>
    <filter id="filter1_f" x="0.677246" y="0.461426" width="79.4677" height="79.1154" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
    <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
    <feGaussianBlur stdDeviation="15" result="effect1_foregroundBlur"/>
    </filter>
    <filter id="filter2_d" x="35" y="3" width="10" height="11" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
    <feOffset dy="2"/>
    <feGaussianBlur stdDeviation="0.5"/>
    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.3 0"/>
    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
    </filter>
    <linearGradient id="paint0_linear" x1="40.1253" y1="7" x2="40.1253" y2="7.83159" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint1_linear" x1="23.9225" y1="11.3101" x2="23.9225" y2="12.1417" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint2_linear" x1="0.415794" y1="0" x2="0.415794" y2="0.831587" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint3_linear" x1="7.41579" y1="39.4321" x2="7.41579" y2="40.2637" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint4_linear" x1="0.415797" y1="0" x2="0.415797" y2="0.83159" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint5_linear" x1="0.415793" y1="0" x2="0.415793" y2="0.831589" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint6_linear" x1="40.1242" y1="72.1684" x2="40.1242" y2="73" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint7_linear" x1="0.415792" y1="0" x2="0.415792" y2="0.831587" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint8_linear" x1="0.415794" y1="0" x2="0.415794" y2="0.831585" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint9_linear" x1="0.415796" y1="0" x2="0.415796" y2="0.831585" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint10_linear" x1="0.415796" y1="0" x2="0.415796" y2="0.831585" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    <linearGradient id="paint11_linear" x1="0.41579" y1="0" x2="0.41579" y2="0.831587" gradientUnits="userSpaceOnUse">
    <stop offset="0.385417" stopColor="#FFBC08"/>
    <stop offset="1" stopColor="#353228"/>
    </linearGradient>
    </defs>
  </svg>
));

export default WinSvg;