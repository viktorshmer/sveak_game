import React, { useEffect } from 'react';
import './index.scss';

import Sector from "../sector";

const Wheel = React.forwardRef(({ sectorsElem, generateWheel }, ref) => {
  useEffect(() => generateWheel(), []);
  return(
    <div className="wheel">
      <div className="mini-circles">
        <div className="mini-circles__item mini-circles__item--circle1"></div>
        <div className="mini-circles__item mini-circles__item--circle2"></div>
        <div className="mini-circles__item mini-circles__item--circle3"></div>
        <div className="mini-circles__item mini-circles__item--circle4"></div>
        <div className="mini-circles__item mini-circles__item--circle5"></div>
        <div className="mini-circles__item mini-circles__item--circle6"></div>
        <div className="mini-circles__item mini-circles__item--circle7"></div>
        <div className="mini-circles__item mini-circles__item--circle8"></div>
        <div className="mini-circles__item mini-circles__item--circle9"></div>
        <div className="mini-circles__item mini-circles__item--circle10"></div>
        <div className="mini-circles__item mini-circles__item--circle11"></div>
        <div className="mini-circles__item mini-circles__item--circle12"></div>
      </div>
      <div className="wheel-in">
        <div className="center-shadow"/>
        <div className="container" ref={ref}>
          {sectorsElem.map(({classElem, backgroundColor, transform}, idx) => (
            <Sector key={idx} classElem={classElem} backgroundColor={backgroundColor} transform={transform}/>
          ))}
        </div>
      </div>
    </div>
  );
});

export default Wheel;