import React from 'react';
import './index.scss';
import arrow from '../../images/arrow.svg';

function Arrow() {
  return (
    <img className="arrow" src={arrow} alt="arrow"/>
  );
}

export default Arrow;