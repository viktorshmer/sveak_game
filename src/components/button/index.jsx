import React from 'react';
import './index.scss';

function SpinButton({clickHandler, title, disabled = false}) {
  return (
    <button className="spin-button" onClick={clickHandler} disabled={disabled}>{title}</button>
  );
}

export default SpinButton;